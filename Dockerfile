FROM node:18-alpine
RUN apk add --no-cache libc6-compat

COPY package.json yarn.lock ./

RUN yarn install --frozen-lockfile
RUN yarn global add nodemon 

COPY .  .

CMD [ "nodemon","src/index.js" ]
